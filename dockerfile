FROM openjdk:alpine
COPY . /opt
WORKDIR /opt/minecraft

# Updating the version zip requires a similar change to the CI version tag
RUN wget https://media.forgecdn.net/files/2544/807/FTBPyramidReborn-1.12.2-2.1.1-Server.zip
RUN unzip FTBPyramidReborn-1.12.2-2.1.1-Server.zip

# this modpack comes with world data, so we need to move it
# somewhere safe so it doesn't get overridden
RUN mv /opt/minecraft/world /opt/minecraft/default_world

RUN chmod 700 FTBInstall.sh
RUN chmod 700 ServerStart.sh
RUN ./FTBInstall.sh
RUN cp /opt/eula.txt /opt/minecraft
RUN cp /opt/entrypoint.sh /opt/minecraft
RUN chmod 700 entrypoint.sh

RUN apk add --no-cache py-pip
RUN pip install mcstatus

HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/entrypoint.sh"]