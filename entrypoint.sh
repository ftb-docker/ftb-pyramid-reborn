#!/bin/sh

# on start we copy in the default world data:
if [ -z "$(ls -A /opt/minecraft/world)" ]; then
    cp /opt/minecraft/default_world/* /opt/minecraft/world/ -rf
fi

/opt/minecraft/ServerStart.sh